using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiGravityZone : MonoBehaviour
{
    public RewindableObject targetObject;
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            if (targetObject.isRewind)
            {
                RewindManager.instance.isAntiGravity = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            RewindManager.instance.isAntiGravity = false;
        }
    }
}
