using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public bool started = false;
    public PlayerControllerMine player;
    public Animator animator;
    public BoxCollider2D col;
    public GameObject bossBullet;
    public List<TravelPoint> travelPoints = new List<TravelPoint>();
    public int currentPoint = 0;
    public float[] attackFreq = new float[] { 5f, 4f, 2f };
    public bool canAttack = true;
    public bool isDamaged = false;
    public float[] moveTime = new float[] { 0.5f, 1f, 2f };
    public int live = 3;
    public bool isDead = false;

    private void Start()
    {
        player = FindObjectOfType<PlayerControllerMine>();
        animator = GetComponentInChildren<Animator>();
        col = GetComponent<BoxCollider2D>();
    }

    void FixedUpdate()
    {
        if (started)
        {
            if (live > 0)
            {
                if (!isDamaged)
                {
                    if (canAttack)
                    {
                        StartCoroutine(Attack(attackFreq[3 - live]));
                    }

                    transform.position = Vector2.Lerp(transform.position, travelPoints[currentPoint].transform.position, Time.fixedDeltaTime * moveTime[3 - live]);
                    if (Vector2.Distance(transform.position, travelPoints[currentPoint].transform.position) <= 0.1f)
                    {
                        currentPoint++;
                        if (currentPoint >= travelPoints.Count)
                        {
                            currentPoint = 0;
                        }
                    }
                }
            }
            else
            {
                if (!isDead)
                {
                    isDead = true;
                    animator.SetBool("isDead", true);
                }
            }
        }
    }

    IEnumerator Attack(float freq)
    {
        canAttack = false;
        //SpawnBoosBullet();
        animator.SetTrigger("isAttack");
        yield return new WaitForSeconds(freq);
        if (!isDamaged)
        {
            canAttack = true;
        }
    }

    public void SpawnBoosBullet()
    {
        GameObject bullet = Instantiate(bossBullet, transform.position, Quaternion.identity);
        bullet.GetComponent<BossBullet>().targetPosition = player.transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("BossKiller") && started && !isDamaged)
        {
            live--;
            animator.SetTrigger("isDamaged");
            col.enabled = false;
            isDamaged = true;
            if (live != 0)
            {
                StartCoroutine(DelayRestartCollider());
            }
        }
    }

    IEnumerator DelayRestartCollider()
    {
        yield return new WaitForSeconds(2.5f);
        col.enabled = true;
        isDamaged = false;
        canAttack = true;
    }
}
