using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimation : MonoBehaviour
{
    public Boss boss;

    public void Start()
    {
        boss = FindObjectOfType<Boss>();
    }
    public void Boss_Shoot()
    {
        boss.SpawnBoosBullet();
    }

    public void DestroyBoss()
    {
        Destroy(boss.gameObject);
    }
}
