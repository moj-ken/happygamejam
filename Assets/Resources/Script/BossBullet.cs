using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBullet : MonoBehaviour
{
    public Vector2 targetPosition;

    public void Start()
    {
        Destroy(gameObject, 5f);
    }

    private void FixedUpdate()
    {
        transform.position = Vector2.Lerp(transform.position, targetPosition, Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Floor"))
        {
            Destroy(gameObject);
        }

        if (collision.transform.CompareTag("Player"))
        {
            RewindManager.instance.player.isDead = true;
            RewindManager.instance.player.controlEnabled = false;
            RewindManager.instance.player.rb.simulated = false;
            RewindManager.instance.player.TriggerDeadAnimation();
            Destroy(gameObject);
        }
    }
}
