using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //public float speed = 20f;
    //public int damage = 40;
    public float explosionRange;
    public Rigidbody2D rb;
    private GameObject player;
    
    void Start()
    {
        StartCoroutine(DelayDestory());
        player = GameObject.Find("Player");
    }

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        // Explosion Check
        if(hitInfo.gameObject.tag == "Floor" || hitInfo.gameObject.tag == "Enemy" || hitInfo.gameObject.tag == "Plant" || hitInfo.gameObject.tag == "BossKiller")
        {
            player.GetComponent<PlayerControllerMine>().PlayExplosionSound();
            CameraEffect.Instance.CameraShake(5f, 0.25f);
 
            if (Vector2.Distance(transform.position, player.transform.position) <= explosionRange)
            {
                RewindManager.instance.ActiveAllRewindObject();
            }
            else
            {
                foreach (RewindableObject obj in RewindManager.instance.rewindObjects)
                {
                    if (Vector2.Distance(transform.position, obj.transform.position) <= explosionRange)
                    {
                        obj.StartRewind();
                    }
                }
            }

            FindObjectOfType<Weapon>().isShoot = true;
            Destroy(gameObject);


        }


        //
        //Enemy enemy = hitInfo.GetComponent<Enemy>();
        //if(enemy != null)
        //{
        //    enemy.TakeDamage(damage);
        //}
    }

    IEnumerator DelayDestory()
    {
        yield return new WaitForSeconds(5f);
        if (gameObject)
        {
            FindObjectOfType<Weapon>().isShoot = true;
            Destroy(gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, explosionRange);
    }
}
