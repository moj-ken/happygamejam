using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Rendering.PostProcessing;

public class CameraEffect : MonoBehaviour
{
    public static CameraEffect Instance { get; private set; }
    private CinemachineVirtualCamera cvc;
    private float shakeTimer,reverseTimer;
    private PostProcessLayer postfx;

    private void Awake()
    {
        Instance = this;
        cvc = GetComponent<CinemachineVirtualCamera>();
        postfx = GameObject.Find("Main Camera").GetComponent<PostProcessLayer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void CameraShake(float intensity,float time)
    {
        CinemachineBasicMultiChannelPerlin cmPerlin = cvc.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        cmPerlin.m_AmplitudeGain = intensity;
        shakeTimer = time;
    }
    
    public void CameraReverse(bool isRewind)
    {
        if (isRewind)
        {
            postfx.enabled = true;
        }
        else
        {
            postfx.enabled = false;
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        if(shakeTimer >0)
        {
            shakeTimer -= Time.deltaTime;
            if(shakeTimer <= 0f)
            {
                CinemachineBasicMultiChannelPerlin cmPerlin = cvc.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                cmPerlin.m_AmplitudeGain = 0f;
            }
        }

    }

}
