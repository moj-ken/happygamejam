using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{
    public PlayerControllerMine character;
    public Weapon weapon;

    private void Start()
    {
        character = GetComponentInParent<PlayerControllerMine>();
        weapon = GetComponentInParent<Weapon>();
    }

    public void Shoot_Animation()
    {
        weapon.Shoot();
    }

    public void Respawn()
    {
        RewindManager.instance.RespawnToLastCheckPoint();
        RewindManager.instance.player.rb.simulated = true;
    }
}
