using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public int checkPointNum;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.root.CompareTag("Player"))
        {
            if (RewindManager.instance.currentCheckPoint < checkPointNum)
            {
                RewindManager.instance.currentCheckPoint = checkPointNum;
            }

            if (RewindManager.instance.player.isDead)
            {
                RewindManager.instance.player.isDead = false;
                RewindManager.instance.player.controlEnabled = true;
            }
        }
    }
}
