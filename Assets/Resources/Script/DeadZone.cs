using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.root.CompareTag("Player"))
        {
            RewindManager.instance.player.isDead = true;
            RewindManager.instance.player.controlEnabled = false;
            RewindManager.instance.player.rb.simulated = false;
            RewindManager.instance.player.TriggerDeadAnimation();
        }
    }
}
