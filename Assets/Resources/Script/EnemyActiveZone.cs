using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActiveZone : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            transform.root.GetComponent<Enemy>().playerInRange = true;
        }
    }
}
