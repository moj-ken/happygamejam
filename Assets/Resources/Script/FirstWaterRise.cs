using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstWaterRise : MonoBehaviour
{
    public Animator waterAnim;
    private bool isFirstRise = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D player)
    {
        // Explosion Check
        if (player.gameObject.tag == "Player" & (isFirstRise))
        {
            waterAnim.SetTrigger("firstRise");
            isFirstRise = false;
        }
    }
}