using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerMine : MonoBehaviour
{
    public Rigidbody2D rb;
    public Animator animator;
    public float maxSpeed = 4f;
    public float jumpTakeOffSpeed = 7;

    public bool controlEnabled;
    public bool isDead = false;
    public Vector2 move;
    public float movement;
    public JumpState jumpState = JumpState.Grounded;
    public float jumpModifier = 1.5f;
    public float jumpDeceleration = 0.5f;
    public bool stopJump = false;
    public bool jump = false;
    public bool canJump = true;
    public bool isGrounded = true;

    private SpriteRenderer spriteRenderer;

    [Header("Sound")]
    private AudioSource audioSource;
    public AudioClip respawnClip;
    public AudioClip explosionClip;
    public AudioClip deadClip;
    public AudioClip shootClip;
    public AudioClip jumpClip;
    public AudioClip windClip;

    void Awake()
    {
        rb = GetComponentInChildren<Rigidbody2D>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        animator = GetComponentInChildren<Animator>();
        audioSource = GetComponentInChildren<AudioSource>();
        isDead = false;
        stopJump = true;
        jump = false;
        canJump = true;
        isGrounded = true;
        PlayRespawnSound();
    }

    // Update is called once per frame
    void Update()
    {
        if (controlEnabled)
        {
            if (!isDead)
            {
                Move();
                CheckJump();
            }
        }
        UpdateJumpState();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Floor") || collision.transform.CompareTag("Plant"))
        {
            isGrounded = true;
            jumpState = JumpState.Grounded;
            animator.SetBool("isJump", false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Floor") || collision.transform.CompareTag("Plant"))
        {
            isGrounded = false;
        }
    }

    public void Move()
    {
        movement = Input.GetAxisRaw("Horizontal");

        //walk
        if (movement > 0.1f)
        {
            spriteRenderer.flipX = false;
            if (rb.velocity.x < maxSpeed)
            {
                rb.AddForce(new Vector2(1.0f * maxSpeed, 0), ForceMode2D.Impulse);
            }
            //rb.AddForce(new Vector2(1.0f * maxSpeed, 0), ForceMode2D.Impulse);
            transform.Translate(maxSpeed * Time.deltaTime, 0, 0);
            animator.SetBool("isWalk", true);
        }
        else if (movement < -0.1f)
        {
            spriteRenderer.flipX = true;
            if (rb.velocity.x > -maxSpeed)
            {
                rb.AddForce(new Vector2(-1.0f * maxSpeed, 0), ForceMode2D.Impulse);
            }
            //rb.AddForce(new Vector2(-1.0f * maxSpeed, 0), ForceMode2D.Impulse);
            transform.Translate(-maxSpeed * Time.deltaTime, 0, 0);
            animator.SetBool("isWalk", true);
        }
        else
        {
            rb.velocity = new Vector2(0f, rb.velocity.y);
            animator.SetBool("isWalk", false);
        }

        /*if (movement < 0.1f && movement > -0.1f)
        {
            if (!jump && isGrounded)
            {
                //idle
            }
        }*/
    }

    void CheckJump()
    {
        if (jumpState == JumpState.Grounded && Input.GetButtonDown("Jump"))
        {
            jumpState = JumpState.PrepareToJump;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            stopJump = true;
        }
    }

    void Jump()
    {
        if (jump && !stopJump && canJump)
        {
            PlayJumpSound();
            canJump = false;
            rb.velocity = new Vector2(rb.velocity.x, jumpTakeOffSpeed);
            animator.SetBool("isJump", true);
        }
    }

    void UpdateJumpState()
    {
        jump = false;
        switch (jumpState)
        {
            case JumpState.PrepareToJump:
                jumpState = JumpState.Jumping;
                jump = true;
                stopJump = false;
                canJump = true;
                Jump();
                break;
            case JumpState.Jumping:
                if (!isGrounded)
                {
                    jumpState = JumpState.InFlight;
                    canJump = false;
                }
                break;
            case JumpState.InFlight:
                if (isGrounded)
                {
                    jumpState = JumpState.Landed;
                    canJump = true;
                }
                break;
            case JumpState.Landed:
                jumpState = JumpState.Grounded;
                canJump = true;
                animator.SetBool("isJump", false);
                break;
        }
    }

    public void TriggerDeadAnimation()
    {
        PlayDeadSound();
        animator.SetTrigger("isDead");
    }

    public enum JumpState
    {
        Grounded,
        PrepareToJump,
        Jumping,
        InFlight,
        Landed
    }

    private void Filp()
    {
        // Switch the way the player is labelled as facing.
        transform.Rotate(0f, 180f, 0f);
    }

    public void PlayRespawnSound()
    {
        audioSource.clip = respawnClip;
        audioSource.Play();
    }

    public void PlayExplosionSound()
    {
        audioSource.clip = explosionClip;
        audioSource.Play();
    }
    public void PlayDeadSound()
    {
        audioSource.clip = deadClip;
        audioSource.Play();
    }
    public void PlayShootSound()
    {
        audioSource.clip = shootClip;
        audioSource.Play();
    }
    public void PlayJumpSound()
    {
        audioSource.clip = jumpClip;
        audioSource.Play();
    }
    public void PlayWindSound()
    {
        audioSource.clip = windClip;
        audioSource.Play();
    }
}
