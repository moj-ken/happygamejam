using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindManager : MonoBehaviour
{
    public static RewindManager instance;
    public PlayerControllerMine player;
    public Rigidbody2D playerRB;
    public List<RewindableObject> rewindObjects = new List<RewindableObject>();
    public float masterRewindTime = 5f;
    public float explosionRange = 5f;
    public float bulletSpeed = 10f;
    public bool isAllRewind = false;

    [Header("Gravity")]
    private Vector2 antiGravityValue = new Vector2(0f, 1f);
    private Vector2 normalGravityValue = new Vector2(0f, -9.81f);
    public bool isAntiGravity = false;

    [Header("Check Point")]
    public List<CheckPoint> checkPoints = new List<CheckPoint>();
    public int currentCheckPoint = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        RefreshRewindObjects();
        ReloadCheckPoints();
    }

    private void Start()
    {
        player = FindObjectOfType<PlayerControllerMine>();
        playerRB = player.GetComponent<Rigidbody2D>();
    }

    public void FixedUpdate()
    {
        if (isAntiGravity)
        {
            AntiGravityForPlayer();
        }
    }

    public void ReloadCheckPoints()
    {
        checkPoints = new List<CheckPoint>();
        CheckPoint[] cp = FindObjectsOfType<CheckPoint>();
        foreach (CheckPoint c in cp)
        {
            checkPoints.Add(c);
        }
        ReorderCheckPoints();
    }

    public void ReorderCheckPoints()
    {
        checkPoints.Sort((x, y) => x.transform.position.x.CompareTo(y.transform.position.x));
        int num = 0;
        foreach (CheckPoint c in checkPoints)
        {
            c.checkPointNum = num;
            num++;
        }
    }

    public void RefreshRewindObjects()
    {
        rewindObjects = new List<RewindableObject>();
        RewindableObject[] objects = FindObjectsOfType<RewindableObject>();
        foreach (RewindableObject obj in objects)
        {
            rewindObjects.Add(obj);
        }
    }

    public void ActiveAllRewindObject()
    {
        CameraEffect.Instance.CameraReverse(true);
        foreach (RewindableObject obj in rewindObjects)
        {
            obj.StartRewind();
        }
    }

    public void DeactiveRewindObject()
    {
        foreach (RewindableObject obj in rewindObjects)
        {
            obj.EndRewind();
        }
    }

    public void RespawnToLastCheckPoint()
    {
        foreach (CheckPoint c in checkPoints)
        {
            if (c.checkPointNum == currentCheckPoint)
            {
                player.transform.position = c.transform.position;
                break;
            }
        }
    }

    public void AntiGravityForPlayer()
    {
        Debug.Log("AntiGravity");
        playerRB.AddForce(antiGravityValue, ForceMode2D.Impulse);
        player.PlayWindSound();
    }

    private void OnLevelWasLoaded(int level)
    {
        player = FindObjectOfType<PlayerControllerMine>();
        playerRB = player.GetComponent<Rigidbody2D>();
        RefreshRewindObjects();
        ReloadCheckPoints();
        currentCheckPoint = 0;
    }
}
