using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindableObject : MonoBehaviour
{
    public bool isRewind = false;
    private bool atEnd = true;
    Animator animator;
    SpriteRenderer spriteRenderer;

    void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    public void StartRewind()
    {
        animator.SetFloat("mpSpeed", -1);
        spriteRenderer.color = Color.red;
        isRewind = true;
        StartCoroutine(BackToNormal());
    }

    public void EndRewind()
    {
        animator.SetFloat("mpSpeed", 1);
        spriteRenderer.color = Color.white;
        isRewind = false;
        atEnd = true;
    }

    public void PauseAnimation()
    {
        if (atEnd)
        {
            atEnd = false;
            animator.SetFloat("mpSpeed", 0);
        }
    }

    IEnumerator BackToNormal()
    {
        yield return new WaitForSeconds(RewindManager.instance.masterRewindTime);
        EndRewind();
        CameraEffect.Instance.CameraReverse(false);
    }
}
