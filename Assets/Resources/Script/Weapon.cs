using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Weapon : MonoBehaviour
{
    private PlayerControllerMine player;
    private Animator playerAnimator;
    public Transform firePoint;
    public GameObject pfBullet;
    public bool isShoot;
    private Camera cam;
    public Vector2 mousePos;
    public Vector2 lookDir;
    public float aimAngle;
    //public LineRenderer lineRenderer;

    void Start()
    {
        cam = FindObjectOfType<Camera>();
        player = FindObjectOfType<PlayerControllerMine>();
        playerAnimator = FindObjectOfType<PlayerControllerMine>().GetComponentInChildren<Animator>();
        isShoot = true ;
    }

    void Update()
    {
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButton(0) && isShoot && player.isGrounded)
        {
            isShoot = false;
            playerAnimator.SetTrigger("isShoot");
        }
    }

    private void FixedUpdate()
    {
        Vector2 temp = mousePos - new Vector2(firePoint.position.x, firePoint.position.y);
        lookDir = temp.normalized;
        aimAngle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
    }

    public void Shoot()
    {
        player.PlayShootSound();
        GameObject bullet = Instantiate(pfBullet, firePoint.position, firePoint.rotation);
        Bullet b = bullet.GetComponent<Bullet>();
        b.explosionRange = RewindManager.instance.explosionRange;
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(lookDir * RewindManager.instance.bulletSpeed, ForceMode2D.Impulse);
        //playerAnimator.SetTrigger("isShoot");

        //RaycastHit2D hitInfo = Physics2D.Raycast(firePoint.position, firePoint.right);

        //if (hitInfo)
        //{
        //    Enemy enemy = hitInfo.transform.GetComponent<Enemy>();
        //    if (enemy != null)

        //    {
        //        enemy.TakeDamage(damage);

        //    }

        //    //Instantiate(pfBullet, firePoint.position, firePoint.rotation);

        //lineRenderer.SetPosition(0, firePoint.position);
        //    lineRenderer.SetPosition(1, hitInfo.point);
        //}
        //else
        //{
        //    lineRenderer.SetPosition(0, firePoint.position);
        //    lineRenderer.SetPosition(1, firePoint.position + firePoint.right * 100);
        //}

        //lineRenderer.enabled = true;

        ////yield return 0;

        ////wait one frame

        //lineRenderer.enabled = false;
        //isShoot = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(firePoint.position, mousePos);
    }
}
